#include "my_class.h"

#include <iostream>

namespace simucene {

//..................................................................................................
void MyClass::hello() const {
    std::cout << "Hello, " << name_ << '\n';
}

}
