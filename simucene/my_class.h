#ifndef SIMUCENE_MY_CLASS_H
#define SIMUCENE_MY_CLASS_H

#include <string>

namespace simucene {

//--------------------------------------------------------------------------------------------------
class MyClass {
public:
    MyClass(std::string const& name): name_(std::move(name)) {}

    void hello() const;

private:
    std::string name_;
};

}

#endif
