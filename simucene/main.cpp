#include "my_class.h"
#include "vector.h"
#include <iostream>

using namespace simucene;

//--------------------------------------------------------------------------------------------------
int main(int const, char const**) {
    MyClass my_class("world");
    my_class.hello();

    Vector3<double> x = {0, 1, 2};
    x = exp(x.array());
    std::cout << x << '\n';

    return 0;
}
